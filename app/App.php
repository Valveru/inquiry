<?php

namespace App;

use Repositories\ContentRepository;
use Services\NotificationService;
use Validation\NumberValidator;

/**
 * Created by PhpStorm.
 * User: mkardakov
 * Date: 11/3/17
 * Time: 11:00 AM
 */
class App
{

    // Проверка $value в изначальном коде никак не связана с загрузкой и сохранением данных, но для исправления этого пункта не хватает данных
    public function doIt($value)
    {
        $repository = new ContentRepository();
        $validator = new NumberValidator(); 
        
        $content = $repository->download();
        $repository->writeContent($content);

        $validator->validate($value);
        if (!$validator->isValid()) {
            $notification_service = NotificationService::getInstance();
            $notification_service->notify($validator->getMessage());
        }
    }

}

