<?php

namespace Storages;

use Services\StorageFactory;
use Models\Content;
use Models\Interfaces\IContent;
use Storages\Interfaces\IContentStorage;

class ContentStorage implements IContentStorage
{
    private $connection;
    
    public function __construct()
    {
        $this->setConnection(); 
    }

    protected function setConnection() {
        $factory = new StorageFactory();
        $this->connection =  $factory->create();
    }
    
    protected function getConnection()
    {
        return $this->connection;
    }
  
    public function read() 
    {
        $string = $this->getConnection()->load();
        $content = new Content(); 
        $content->setText($string); 
        return $content; 
    }
    public function write(IContent $content) 
    { 
        $this->getConnection()->save($content->getText());  
    }
}
