<?php

namespace Storages\Interfaces;

use Models\Interfaces\IContent; 

interface IContentStorage
{
    public function read();
	public function write(IContent $content);
}
