<?php

spl_autoload_extensions(".php");
spl_autoload_register(function ($class) {
    include './' . $class . '.php';
});
  
try {
    $app = new \App\App();
    $app->doIt(4);
} catch (\Exceptions $e) {
    $e->getMessage(); 
    exit; 
}

