<?php

namespace Drivers;

use Drivers\Interfaces\ITransport;
use Drivers\Interfaces\ISendTransport;

class EmailDriver implements ISendTransport, ITransport
{
    public function send($body, $to, $from)
    {
        printf("Email has been send to %s From %s.\r\n\r\n Notify you about %s", $to, $from, $body);
    }

}
