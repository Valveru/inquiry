<?php

namespace Drivers;

use Exceptions\ConnectionException;
use Exceptions\DriverException;
use Services\ConfigService;
use Drivers\Interfaces\ILoadConnection;
use Drivers\Interfaces\IConnection;
use Drivers\Interfaces\ITransport;

class CurlDriver implements ILoadConnection, IConnection, ITransport
{
    private $url;
    
    public function __construct()
    {
        $config = ConfigService::getInstance();
        $this->url = $config->get('curl.url');
        if (empty($this->url)) {
            throw new DriverException('Incorrect driver configuration');
        }
    }

    public function load() 
    {
        $CH = curl_init($this->url);

        if ($CH === false) {
            throw new ConnectionException('Can not establish a remote connection');
        }
        
        curl_setopt($CH, CURLOPT_RETURNTRANSFER, true);
        $body = curl_exec($CH);
        if ($CH === false) {
            throw new ConnectionException('Can not establish a remote connection');
        }
        curl_close($CH);
        return $body;
    }

}
