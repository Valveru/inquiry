<?php

namespace Drivers;

use Services\ConfigService;
use Drivers\Interfaces\ILoadConnection;
use Drivers\Interfaces\ISaveConnection;
use Drivers\Interfaces\IConnection;
use Drivers\Interfaces\ITransport;
use Exceptions\DriverException;

class FileDriver implements ILoadConnection, ISaveConnection, IConnection, ITransport
{
    private $file_name; 
    
    public function __construct()
    {
        $config = ConfigService::getInstance();
        $this->file_name = $config->get('file.file_name'); 
        if (empty($this->file_name)) {
            throw new DriverException('Incorrect driver configuration');
        }
    }

    public function load()// : string 
    {
        $string = file_get_contents($this->file_name);
        if ($string === false) {
            throw new DriverException('File not found');
        }
        return $string;
    }

    public function save($text)
    {
        $string = file_put_contents($this->file_name, $text, FILE_APPEND);
        if ($string === false) {
            throw new DriverException('Can not write to file');
        }
    }
}
