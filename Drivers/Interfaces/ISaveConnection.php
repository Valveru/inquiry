<?php

namespace Drivers\Interfaces;

interface ISaveConnection
{

    public function save($text);
}
