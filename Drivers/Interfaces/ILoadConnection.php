<?php

namespace Drivers\Interfaces;

interface ILoadConnection
{

    public function load();
}
