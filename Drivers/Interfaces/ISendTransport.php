<?php

namespace Drivers\Interfaces;

interface ISendTransport
{

    public function send($to, $from, $message);
}
