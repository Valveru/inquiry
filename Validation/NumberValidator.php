<?php

namespace Validation;

use Exceptions\GeneralException;

class NumberValidator extends Validator
{
    public function validate($value)
    {
        if (!is_numeric($value)) {
            $this->setMessage('Value has not valid format');
            $this->setInvalid();
        } elseif ($value >= 3 && $value < 6) {
            $this->setMessage('Your Value is too low');
            $this->setInvalid();
        } elseif ($value == 7) {
            $this->setMessage('Your Value equals to 7');
            $this->setInvalid();
        } else {
            $this->setValid();
        }

        return $this->isValid();
    }
}
