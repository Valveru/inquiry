<?php

namespace Validation;

use Exceptions\GeneralException;

abstract class Validator
{
    protected $cur_message = '';
    protected $valid = null;  
    
    abstract public function validate($value); 

    public function getMessage() 
    {
        return $this->cur_message; 
    }

    public function setMessage($message)
    {
        $this->cur_message = $message;
    }
    
    protected function setValid()
    {
        $this->valid = true; 
    }

    protected function setInvalid()
    {
        $this->valid = false;
    }

    public function isValid()
    {
        if (!isset($this->valid)) {
            throw new GeneralException('Validate before'); 
        }
        return $this->valid; 
    }
}
