<?php

namespace Repositories;

use Models\Interfaces\IContent;
use Storages\ContentStorage; 
use Repositories\Interfaces\IRepository;
use Transports\ContentTransport;

class ContentRepository implements IRepository
{
    private $storage;
    private $transport; 
    
    public function __construct() 
    {
        $this->storage = new ContentStorage; 
        $this->transport = new ContentTransport; 
    }
    public function readContent()
    {
        return $this->storage->read(); 
    }
    
    public function writeContent(IContent $content) 
    { 
        $this->storage->write($content);  
    }
    
    public function download()
    {
        return $this->transport->get();
    }
}
