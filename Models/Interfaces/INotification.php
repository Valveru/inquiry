<?php

namespace Models\Interfaces;


interface INotification
{
    public function getMessage (); 
	public function setMessage ($message);
    public function getSender ();
    public function setSender ($sender);
    public function getRecipient ();
    public function setRecipient ($recipient);
}
