<?php

namespace Models\Interfaces;


interface IContent
{
    public function getText (); 
	public function setText ($text);
}
