<?php

namespace Models;

use Models\Interfaces\IContent; 

class Content implements IContent
{
    private $text; 

    public function getText() 
    {
        return $this->text; 
    }
    
    public function setText( $text)
    {
        $this->text = $text; 
    }
}
