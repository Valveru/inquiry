<?php

namespace Models;

use Models\Interfaces\INotification;

class Notification implements INotification
{
    protected $message;
    protected $sender;
    protected $recipient;

    public function getMessage() 
    {
        return $this->message;
    }
    
    public function setMessage( $message)
    {
        $this->message = $message;
    }



    public function getSender() 
    {
        return $this->sender;
    }

    public function setSender($sender)
    {
        $this->sender = $sender;
    }
    

    public function getRecipient() 
    {
        return $this->recipient;
    }

    public function setRecipient($recipient)
    {
        $this->recipient = $recipient;
    }
}
