<?php

namespace Exceptions;

use Exception;

class ConnectionException extends Exception
{
    public function __construct($message, $code = 0, Exception $previous = null) {
        parent::__construct($message, $code, $previous);
    }

    public function __toString() {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n {$this->getLastErrorMessage()}";
    }

    private function getLastErrorMessage ()
    {
        $error_data = error_get_last();
        if (!empty($error_data)) {
            return $error_data['message'];
        }
        return '';
    }
}
