<?php

namespace Transports;

use Models\IContent;
use Models\Content;
use Services\TransportFactory;
use Transports\Interfaces\IContentTransport;

class ContentTransport implements IContentTransport
{
    private $connection;
    
    function __construct()
    {
        $this->setConnection(); 
    }

    protected function setConnection()
    {
        $factory = new TransportFactory();
        $this->connection =  $factory->create();
    }
    
    protected function getConnection()
    {
        return $this->connection; 
    }

    public function get() 
    {
        $string = $this->getConnection()->load();
        $content = new Content();
        $content->setText($string);
        return $content;
    }

}
