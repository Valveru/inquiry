<?php

namespace Transports\Interfaces;

use Models\Interfaces\INotification; 

interface INotificationTransport
{
    public function send(INotification $notification);
}
