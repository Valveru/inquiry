<?php

namespace Transports\Interfaces;

use Models\Interfaces\IContent; 

interface IContentTransport
{
    public function get();
}
