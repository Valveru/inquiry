<?php

namespace Transports;

use Models\Interfaces\INotification;
use Services\NotificationFactory;
use Transports\Interfaces\INotificationTransport;

class NotificationTransport implements INotificationTransport
{
    private $connection;
    
    public function __construct()
    {
        $this->setConnection(); 
    }

    protected function setConnection()
    {
        $factory = new NotificationFactory();
        $this->connection =  $factory->create();
    }

    protected function getConnection()
    {
        return $this->connection;
    }

    public function send(INotification $notification) 
    {
        $this->getConnection()->send(
            $notification->getMessage(), 
            $notification->getRecipient(), 
            $notification->getSender()
        );
    }

}
