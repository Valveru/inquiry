<?php

namespace Config;

use Exceptions\GeneralException; 

class Config 
{
    const configPath = __DIR__ . '/config.ini';

    private $config;


    public function __construct()
    {
        $this->config = parse_ini_file(self::configPath, true);
        if ($this->config === false) {
            throw new GeneralException('Application config not found'); 
        }
    }
    
    public function get($section_path)
    {
        $data_path = explode('.', $section_path);

        $cur_path = &$this->config;

        foreach ($data_path as $key) {
            if (!isset($cur_path[$key])) {
                break;
            }
            if (is_string($cur_path[$key])) {
                return $cur_path[$key];
            }
            if (is_array($cur_path[$key])) {
                $cur_path = &$cur_path[$key];
            }
        }
        return null; 
    }
}
