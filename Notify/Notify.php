<?php

namespace Notify;

use Exceptions\GeneralException;
use Models\Notification;
use Services\ConfigService;
use Transports\NotificationTransport;

class Notify
{
    protected $transport;
    protected $from;
    protected $to;

    public function __construct()
    {
        $this->transport = new NotificationTransport();
       
        $config = ConfigService::getInstance();
        $this->from = $config->get('notification.from');
        $this->to = $config->get('notification.to');
        if (!isset($this->from) || !isset($this->to)) {
            new GeneralException('Notifications were not configured');
        }
    }

    public function notify($message)
    {
        $notification = new Notification(); 
        
        $notification->setMessage($message);
        $notification->setRecipient($this->to);
        $notification->setSender($this->from);
        
        $this->transport->send($notification);
    }
}
