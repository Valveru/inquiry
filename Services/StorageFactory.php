<?php
namespace Services;

use Drivers\DbDriver;
use Drivers\FileDriver;
use Drivers\Interfaces\IConnection;
use Exceptions\GeneralException;

final class StorageFactory extends FactoryMethod
{
    protected $type = 'storage';
    
    protected function getInstance ($driver_name) // :IConnection
    {
        switch ($driver_name) {
            case 'db':
                return new DbDriver();
            case 'file':
                return new FileDriver();
            default:
                throw new GeneralException('Storage not found');
        }
    }
}
