<?php
namespace Services;

use Config\Config;

final class ConfigService
{
    private static $config;
    public static function getInstance ()
    {
        if (null === static::$config) {
            static::$config = new Config();
        }
        return static::$config;
    }

    private function __construct()
    {
    }

    private function __clone()
    {
    }

    private function __wakeup()
    {
    }
}
