<?php

namespace Services;

use Exceptions\GeneralException;

abstract class FactoryMethod
{
    protected $type = null;
    abstract protected function getInstance($driver_name);

    public function create()
    {
        if (!isset($this->type)) {
            throw new GeneralException('Not configured');
        }
        $config = ConfigService::getInstance();
        $driver_name = $config->get('general.default_' . $this->type . '_driver');
        $obj = $this->getInstance($driver_name);
        return $obj;
    }
}
