<?php
namespace Services;

use Notify\Notify;

final class NotificationService
{
    private static $notification;
    public static function getInstance ()
    {
        if (null === static::$notification) {
            static::$notification = new Notify();
        }
        return static::$notification;
    }

    private function __construct()
    {
    }

    private function __clone()
    {
    }

    private function __wakeup()
    {
    }
}
