<?php
namespace Services;

use Drivers\CurlDriver;
use Drivers\Interfaces\ITransport;
use Exceptions\GeneralException;

final class TransportFactory extends FactoryMethod
{
    protected $type = 'transport';

    public function getInstance ($driver_name) // :ITransport
    {
        switch ($driver_name) {
            case 'curl':
                return new CurlDriver();
            default:
                throw new GeneralException('Transport not found');
        }
    }
}
