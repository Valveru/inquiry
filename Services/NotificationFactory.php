<?php
namespace Services;

use Drivers\EmailDriver;
use Exceptions\GeneralException;
use Drivers\Interfaces\ITransport; 

final class NotificationFactory extends FactoryMethod
{
    protected $type = 'notify';

    public function getInstance ($driver_name) // :ITransport
    {
        switch ($driver_name) {
            case 'email':
                return new EmailDriver();
            default:
                throw new GeneralException('Transport not found');
        }
    }
}
